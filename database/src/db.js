import sqlite from "sqlite";
import SQL from "sql-template-strings";

const initializeDatabase = async () => {
  const db = await sqlite.open("./db.sqlite");

  /**
   * creates a contact
   * @param {object} props an object with keys `name` and `email`
   * @returns {number} the id of the created contact (or an error if things went wrong)
   */
  const createTv = async props => {
    if (
      !props ||
      !props.pdate ||
      !props.status ||
      !props.ttype ||
      !props.size
    ) {
      throw new Error(
        `you must provide the production date, status, type and size`
      );
    }
    const { pdate, status, ttype, size } = props;
    try {
      const result = await db.run(
        SQL`INSERT INTO tvs (pdate, status, ttype ,size) VALUES (${pdate}, ${status}, ${ttype}, ${size})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };

  /**
   * deletes a contact
   * @param {number} id the id of the contact to delete
   * @returns {boolean} `true` if the contact was deleted, an error otherwise
   */
  const deleteTv = async id => {
    try {
      const result = await db.run(SQL`DELETE FROM tvs WHERE tv_id = ${id}`);
      if (result.stmt.changes === 0) {
        throw new Error(`tv "${id}" does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't delete the TV "${id}": ` + e.message);
    }
  };

  /**
   * Edits a contact
   * @param {number} id the id of the contact to edit
   * @param {object} props an object with at least one of `name` or `email`
   */
  const updateTv = async (id, props) => {
    if (!props || !(props.pdate || props.status || props.ttype || props.size)) {
      throw new Error(
        `you must provide the production date or the status or type or the size`
      );
    }
    const { pdate, status, ttype, size } = props;
    try {
      let statement = "";
      if (pdate && status && ttype && size) {
        statement = SQL`UPDATE tvs SET pdate=${pdate}, status=${nastatusme}, ttype=${ttype}, size=${size} WHERE tv_id = ${id}`;
      } else if (pdate) {
        statement = SQL`UPDATE tvs SET pdate=${pdate} WHERE tv_id = ${id}`;
      } else if (status) {
        statement = SQL`UPDATE tvs SET status=${status} WHERE tv_id = ${id}`;
      } else if (ttype) {
        statement = SQL`UPDATE tvs SET ttype=${ttype} WHERE tv_id = ${id}`;
      } else if (size) {
        statement = SQL`UPDATE tvs SET size=${size} WHERE tv_id = ${id}`;
      }
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the TV ${id}: ` + e.message);
    }
  };

  /**
   * Retrieves a contact
   * @param {number} id the id of the contact
   * @returns {object} an object with `name`, `email`, and `id`, representing a contact, or an error
   */
  const getTv = async id => {
    try {
      const tvsList = await db.all(
        SQL`SELECT tv_id AS id, pdate, status, ttype, size FROM tvs WHERE tv_id = ${id}`
      );
      const tv = tvsList[0];
      if (!tv) {
        throw new Error(`tv ${id} not found`);
      }
      return tv;
    } catch (e) {
      throw new Error(`couldn't get the tv ${id}: ` + e.message);
    }
  };

  /**
   * retrieves the contacts from the database
   * @param {string} orderBy an optional string that is either "name" or "email"
   * @returns {array} the list of contacts
   */
  const getTvCollection = async orderBy => {
    try {
      let statement = `SELECT tv_id AS id, pdate, status, ttype, size FROM tvs`;
      switch (orderBy) {
        case "pdate":
          statement += ` ORDER BY pdate`;
          break;
        case "status":
          statement += ` ORDER BY status`;
          break;
        case "ttype":
          statement += ` ORDER BY ttype`;
          break;
        case "size":
          statement += ` ORDER BY size`;
          break;
        default:
          break;
      }
      const rows = await db.all(statement);
      if (!rows.length) {
        throw new Error(`no rows found`);
      }
      return rows;
    } catch (e) {
      throw new Error(`couldn't retrieve TVs: ` + e.message);
    }
  };

  const controller = {
    createTv,
    deleteTv,
    updateTv,
    getTv,
    getTvCollection
  };

  return controller;
};

export default initializeDatabase;
