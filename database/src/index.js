import app from "./app";
import initializeDatabase from "./db";

const start = async () => {
  const controller = await initializeDatabase();

  app.get("/", (req, res, next) => res.send("ok"));

  // CREATE
  app.get("/tvs/new", async (req, res, next) => {
    try {
      const { pdate, status, ttype, size } = req.query;
      const result = await controller.createTv({ pdate, status, ttype, size });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // READ
  app.get("/tvs/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const Tv = await controller.getTv(id);
      res.json({ success: true, result: Tv });
    } catch (e) {
      next(e);
    }
  });

  // DELETE
  app.get("/tvs/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteTv(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // UPDATE
  app.get("/tvs/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { pdate, status, ttype, size } = req.query;
      const result = await controller.updateContact(id, {
        pdate,
        status,
        ttype,
        size
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // LIST
  app.get("/tvs/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const Tvs = await controller.getTvCollection(order);
      res.json({ success: true, result: Tvs });
    } catch (e) {
      next(e);
    }
  });

  // Error
  app.use((err, req, res, next) => {
    console.error(err);
    const message = err.message;
    res.status(500).json({ success: false, message });
  });

  app.listen(8080, () => console.log("server listening on port 8080"));
};

start();
