import React, { Component } from "react";
import "./App.css";

import Tv from "./Tv";

class App extends Component {
  state = {
    tvs_list: [],
    error_message: "",
    pdate: "",
    status: "",
    ttype: "",
    size: ""
  };
  getTv = async id => {
    // check if we already have the tv
    const previous_tv = this.state.tvs_list.find(tv => tv.id === id);
    if (previous_tv) {
      return; // do nothing, no need to reload a tv we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/tvs/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of tvs
        const tv = answer.result;
        const tvs_list = [...this.state.tvs_list, tv];
        this.setState({ tvs_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  deleteTvt = async id => {
    try {
      const response = await fetch(`http://localhost:8080/tvs/delete/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // remove the tv from the current list of tvs
        const tvs_list = this.state.tvs_list.filter(tv => tv.id !== id);
        this.setState({ tvs_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  updateTv = async (id, props) => {
    try {
      if (
        !props ||
        !(props.pdate || props.status || props.ttype || props.size)
      ) {
        throw new Error(
          `you need at least Production date, or status or type or size properties to update a tv`
        );
      }
      const response = await fetch(
        `http://localhost:8080/tvs/update/${id}?pdate=${props.pdate}&status=${
          props.status
        }&ttype=${props.ttype}&size=${props.size}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we update the tv, to reproduce the database changes:
        const tvs_list = this.state.tvs_list.map(tv => {
          // if this is the tv we need to change, update it. This will apply to exactly
          // one tv
          if (tv.id === id) {
            const new_tv = {
              id: tv.id,
              pdate: props.pdate || tv.pdate,
              status: props.status || tv.status,
              ttype: props.ttype || tv.ttype,
              size: props.size || tv.size
            };
            return new_tv;
          }
          // otherwise, don't change the tv at all
          else {
            return tv;
          }
        });
        this.setState({ tvs_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  createTv = async props => {
    try {
      if (
        !props ||
        !(props.pdate && props.status && props.ttype && props.size)
      ) {
        throw new Error(
          `you need both name and email properties to create a contact`
        );
      }
      const { pdate, status, ttype, size } = props;
      const response = await fetch(
        `http://localhost:8080/tvs/new/?pdate=${pdate}&status=${status}&ttype${ttype}&size=${size}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the tv that was created in the database, locally
        const id = answer.result;
        const tv = { pdate, status, ttype, size, id };
        const tvs_list = [...this.state.tvs_list, tv];
        this.setState({ tvs_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  getTvCollection = async order => {
    try {
      const response = await fetch(
        `http://localhost:8080/tvs/list?order=${order}`
      );
      const answer = await response.json();
      if (answer.success) {
        const tvs_list = answer.result;
        this.setState({ tvs_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  componentDidMount() {
    this.getTvCollection();
  }
  onSubmit = evt => {
    // stop the form from submitting:
    evt.preventDefault();
    // extract  from state
    const { pdate, status, ttype, size } = this.state;
    // create the tv
    this.createTv({ pdate, status, ttype, size });
    // empty  so the text input fields are reset
    this.setState({ pdate: "", status: "", ttype: "", size: "" });
  };
  render() {
    const { tvs_list, error_message } = this.state;
    return (
      <div className="App">
        {error_message ? <p> ERROR! {error_message}</p> : false}
        {tvs_list.map(tv => (
          <Tv
            key={tv.id}
            id={tv.id}
            pdate={tv.pdate}
            status={tv.status}
            ttype={tv.ttype}
            size={tv.size}
            updateTv={this.updateTv}
            deleteTv={this.deleteTv}
          />
        ))}
        <form className="third" onSubmit={this.onSubmit}>
          <input
            type="text"
            placeholder="Production date"
            onChange={evt => this.setState({ pdate: evt.target.value })}
            value={this.state.pdate}
          />
          <input
            type="text"
            placeholder="TV Status"
            onChange={evt => this.setState({ status: evt.target.value })}
            value={this.state.status}
          />
          <input
            type="text"
            placeholder="TV Type"
            onChange={evt => this.setState({ ttype: evt.target.value })}
            value={this.state.ttype}
          />
          <input
            type="text"
            placeholder="TV Size"
            onChange={evt => this.setState({ size: evt.target.value })}
            value={this.state.size}
          />
          <div>
            <input type="submit" value="ok" />
            <input type="reset" value="cancel" className="button" />
          </div>
        </form>
      </div>
    );
  }
}

export default App;
