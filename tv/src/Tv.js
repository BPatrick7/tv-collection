import React from "react";

export default class Tv extends React.Component {
  state = {
    editMode: false
  };
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };
  renderEditMode() {
    const { id, pdate, status, ttype, size, updateTv, deleteTv } = this.props;
    return <div> edit mode</div>;
  }
  renderViewMode() {
    const { id, pdate, status, ttype, size, deleteTv } = this.props;
    return (
      <div>
        <span>
          {id} - {pdate}
        </span>
        <button onClick={this.toggleEditMode} className="success">
          edit
        </button>
        <button onClick={() => deleteTv(id)} className="warning">
          x
        </button>
      </div>
    );
  }
  renderEditMode() {
    const { pdate, status, ttype, size } = this.props;
    return (
      <form
        className="third"
        onSubmit={this.onSubmit}
        onReset={this.toggleEditMode}
      >
        <input
          type="text"
          placeholder="Production Date"
          pdate="tv_pdate_input"
          defaultValue={pdate}
        />
        <input
          type="text"
          placeholder="TV Status"
          status="tv_status_input"
          defaultValue={status}
        />
        <input
          type="text"
          placeholder="TV Type"
          ttype="tv_ttype_input"
          defaultValue={ttype}
        />
        <input
          type="text"
          placeholder="TV Size"
          size="tv_size_input"
          defaultValue={size}
        />
        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" className="button" />
        </div>
      </form>
    );
  }
  onSubmit = evt => {
    // stop the page from refreshing
    evt.preventDefault();
    // target the form
    const form = evt.target;
    // extract the two inputs from the form
    const tv_pdate_input = form.tv_pdate_input;
    const tv_status_input = form.tv_status_input;
    const tv_ttype_input = form.tv_ttype_input;
    const tv_size_input = form.tv_size_input;
    // extract the values
    const pdate = tv_pdate_input.value;
    const status = tv_status_input.value;
    const ttype = tv_ttype_input.value;
    const size = tv_size_input.value;
    // get the id and the update function from the props
    const { id, updateTv } = this.props;
    // run the update contact function
    updateTv(id, { pdate, status, ttype, size });
    // toggle back view mode
    this.toggleEditMode();
  };
  render() {
    const { editMode } = this.state;
    if (editMode) {
      return this.renderEditMode();
    } else {
      return this.renderViewMode();
    }
  }
}
